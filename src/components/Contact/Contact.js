import React from 'react';
import './Contact.css';
const Contact=props=>{
return(
    <div className="Contact" onClick={props.click}>
        <img src={props.imgLink} width='120px' height="120px" alt=""/>
        <h5>{props.contName}</h5>
    </div>
)
};
export default Contact;