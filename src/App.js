import React, {Component, Fragment} from "react";
import MainMenu from "./containers/MainMenu/MainMenu";
import {NavLink} from "react-router-dom";
import {Route, Switch} from "react-router";
import NewContact from "./containers/NewContact/NewContact";


class App extends Component {
    render() {
        return (
            <Fragment>
                <div className="MainMenu">
                    <div className="MainMenu--header"><h2>Contacts</h2>

                        <p><NavLink className="MainMenu--nav" to="add-contact" exact>Add new Contact</NavLink></p>
                    </div>
                </div>


                <Switch>


                    <Route path='/add-contact' exact component={NewContact}/>
                    <Route path='/edit-contact' exact component={NewContact}/>
                    <Route path='/' exact component={MainMenu}/>
                    <Route render={() => <h1>Not found</h1>}/>

                </Switch>
            </Fragment>
        );
    }
}

export default App;
