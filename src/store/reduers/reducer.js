import * as actionTypes from "../actions/action";
const initialState = {

    contacts: {},
    request: false,
    error: false,
    modal: false,
    modalObj: {},
    edit: {},
    editor: false

};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.DATA_REQUEST:
            return {...state, contacts: {...state.contacts}, request: true, error: false, editor: false};
        case actionTypes.DATA_SUCCESS: {
            return {...state, contacts: action.data, editor: false,edit: {}}
        }

        case actionTypes.DATA_ERROR:
            return {...state, contacts: {...state.contacts}, request: false, error: true, editor: false};
        case actionTypes.SHOW_MODAL:
            return {...state, modal: true, modalObj: action.modObj,editor:false};
        case actionTypes.HIDE_MODAL:
            return {...state, modal: false, modalObj: {}};
        case actionTypes.EDITOR:{
            console.log(state.edit);
            return {...state, editor: true, edit: action.obj}}

        default:
            return state
    }
};
export default reducer;