import axios from "../../axios-orders";

export const DATA_REQUEST = 'DATA_REQUEST';
export const DATA_SUCCESS = 'DATA_SUCCESS';
export const DATA_SUCCESS_ADD = 'DATA_SUCCESS_ADD';
export const DATA_ERROR = 'DATA_ERROR';
export const SHOW_MODAL = 'SHOW_MODAL';
export const HIDE_MODAL = 'HIDE_MODAL';
export const EDITOR = 'EDITOR';

export const dataRequest = () => {
    return {type: DATA_REQUEST}
};
export const dataSuccess = (data) => {
    return {type: DATA_SUCCESS, data}
};
export const dataSuccessAdd = () => {
    return {type: DATA_SUCCESS_ADD}
};
export const dataError = () => {
    return {type: DATA_ERROR}
};
export const getContacts = () => {

    return (dispatch, getState) => {
        dispatch(dataRequest());
        axios.get('/contacts.json').then(response => {
            dispatch(dataSuccess(response.data));

        }, error => {
            dispatch(dataError())
        })
    }
};

export const addNewContact = (elem) => {

    return (dispatch, getState) => {
        dispatch(dataRequest());
        axios.post('/contacts.json', elem).then(() => {
                dispatch(dataSuccessAdd());
                dispatch(getContacts())
                    , error => {
                    dispatch(dataError());

                };
            }
        )
    }
};

export const deleteContact = (id) => {

    return (dispatch, getState) => {
        dispatch(dataRequest());
        console.log(id);
        axios.delete(`/contacts/${id}.json`).then(() => {
            dispatch(hideModal());
            dispatch(dataSuccessAdd());
            dispatch(getContacts())
                , error => {
                dispatch(dataError());

            };

        });
    }
};

export const editContact = (id, elem) => {
    return (dispatch, getState) => {
        dispatch(dataRequest());
        console.log(id,elem);
        axios.put(`/contacts/${id}.json`, elem).then(() => {
            dispatch(hideModal());
            dispatch(dataSuccessAdd());
            dispatch(getContacts())
                , error => {
                dispatch(dataError());

            };

        });

    }
};

export const showModal = (obj) => {
    return {type: SHOW_MODAL, modObj: obj}
};
export const hideModal = () => {
    return {type: HIDE_MODAL}
};

export const editPost = (obj) => {
    console.log(obj);
    return {type: EDITOR, obj}
};