import React, {Component} from "react";
import "./Editor.css";
import {deleteContact, editPost, hideModal} from "../../store/actions/action";
import {connect} from "react-redux";

class Editor extends Component {

    edit = () => {
        this.props.editPost(this.props.modalObj);
        this.props.editClick()
    };


    render() {

        return (
            <div className="Editor">
                <p onClick={this.props.hideMod} className="Editor--close">x</p>
                <div className="Editor--info">
                    <img src={this.props.modalObj.photo}/>
                    <div>
                        <h2>{this.props.modalObj.name}</h2>
                        <p>{this.props.modalObj.phone}</p>
                        <p>{this.props.modalObj.email}</p>
                    </div>
                </div>
                <button onClick={this.edit}>Edit</button>
                <button onClick={() => this.props.deleteContact(this.props.modalObj.id)}>Delete</button>
            </div>
        )
    }

}
const mapStateToProps = state => {


    return {
        modalObj: state.reducer.modalObj
    }
};

const mapDispatchToProps = dispatch => {
    return {
        hideMod: () => dispatch(hideModal()),
        deleteContact: (id) => dispatch(deleteContact(id)),
        editPost: (obj) => dispatch(editPost(obj))
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(Editor);