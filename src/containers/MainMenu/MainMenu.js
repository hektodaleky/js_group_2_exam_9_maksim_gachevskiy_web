import React, {Component, Fragment} from "react";
import {connect} from "react-redux";
import Contact from "../../components/Contact/Contact";

import "./MainMenu.css";
import {getContacts, hideModal, showModal} from "../../store/actions/action";
import Modal from "../../UI/Modal/Modal";
import Editor from "../Editor/Editor";
import Spinner from "../../UI/Spinner/Spinner";
class MainMenu extends Component {

    editPost = () => {
        this.props.history.push({
            pathname: '/edit-contact'
        });
        this.props.hideMod();

    };


    hideModal = () => {
        this.setState({modal: false})

    };

    componentDidMount() {
        this.props.getContacts();
    }

    render() {


        let allCont = [];
        Object.keys(this.props.contacts).map((elem, i) => {
            let oneItem = {...this.props.contacts[elem], id: elem};
            allCont.push(
                <Contact key={oneItem.id} imgLink={oneItem.photo}
                         contName={oneItem.name} click={() => this.props.showMod(oneItem)}/>
            )

        });

            return (

                <div className="MainMenu">
                    <Modal show={this.props.modal} clickHide={this.props.hideMod}>
                        <div><Editor editClick={this.editPost}/></div>
                    </Modal>
                    <div className="MainMenu--contacts">
                        {allCont}</div>
                </div>)
    }
}

const mapStateToProps = state => {
    return {
        contacts: state.reducer.contacts,
        modal: state.reducer.modal,
        request: state.request
    }
};

const mapDispatchToProps = dispatch => {
    return {
        getContacts: () => dispatch(getContacts()),
        showMod: (obj) => dispatch(showModal(obj)),
        hideMod: () => dispatch(hideModal())
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(MainMenu);