import React, {Component} from "react";
import {connect} from "react-redux";

import "./NewContact.css";
import {addNewContact, editContact} from "../../store/actions/action";
class NewContact extends Component {


    state = {

        name: '',
        phone: '',
        email: '',
        photo: ''

    };

    componentDidMount() {
        console.log(this.props.edit);
        this.setState({...this.props.edit});
    }

    changeInfo = (event, name) => {
        this.setState({[name]: event.target.value})
    };
    backToContacts = () => {
        this.props.history.push({
            pathname: '/'
        });
    };

    render() {
        return (
            <div className="NewContact">
                <h3>Add New Contact</h3>
                <input type="text" placeholder="Name" value={this.state.name}
                       onChange={(event) => this.changeInfo(event, 'name')}/>
                <input type="number" placeholder="Phone" value={this.state.phone}
                       onChange={(event) => this.changeInfo(event, 'phone')}/>
                <input type="email" placeholder="Email" value={this.state.email}
                       onChange={(event) => this.changeInfo(event, 'email')}/>
                <input placeholder="Photo" value={this.state.photo}
                       onChange={(event) => this.changeInfo(event, 'photo')}/>
                <img src={this.state.photo} width="150px"
                     height="150px" alt=""
                />

                <div className="NewContact--buttons">
                    <button onClick={() => {
                        if (this.props.editor) {
                            this.props.editContact(this.props.edit.id,
                                {
                                    name: this.state.name, phone: this.state.phone,
                                    email: this.state.email, photo: this.state.photo
                                }
                            );
                            console.log("EDIT")
                        }
                        else {
                            this.props.addNewContact(this.state);
                            console.log("ADD")
                        }


                        this.backToContacts()
                    }}>Save
                    </button>

                    <button onClick={this.backToContacts}>Back to contacts</button>
                </div>
            </div>
        )
    }

}
const mapStateToProps = state => {
    return {
        editor: state.reducer.editor,
        edit: state.reducer.edit,

    }
};

const mapDispatchToProps = dispatch => {
    return {
        addNewContact: (contact) => {
            dispatch(addNewContact(contact))
        },
        editContact: (id,contact) => {
            dispatch(editContact(id,contact))
        }
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(NewContact);